import torch
import numpy as np
from scipy.stats import t, laplace
import matplotlib.pyplot as plt
from quantization.quantizers.fp8_quantizer import FPQuantizer, generate_all_values_fp1, generate_all_float_values_scaled
import torch.optim as optim
from tqdm import tqdm
from ranger_adabelief import RangerAdaBelief  # 导入RangerAdaBelief优化器

# 设置随机种子以保持结果一致性
torch.manual_seed(48)

# 数据点数量
num_samples = 10000

# 生成不同的分布数据 -10~10
uniform_data = torch.FloatTensor(num_samples).uniform_(-10, 10)
# 均值为0，标准差为10/3，这样99.7%的数据将在-10到10内
gaussian_data = torch.randn(num_samples) * 0.5
# 使用自由度13，尽量保证数据的集中性，可以适当调整scale参数来控制尺度
t_distribution_data1 = torch.from_numpy(t.rvs(100, size=num_samples).astype(np.float32))
t_distribution_data2 = torch.from_numpy(t.rvs(1, size=num_samples).astype(np.float32))
t_distribution_data3 = torch.from_numpy(t.rvs(10, size=num_samples).astype(np.float32))
# 均值（位置）为0，scale为10/3
laplace_data = torch.from_numpy(laplace.rvs(loc=0, size=num_samples).astype(np.float32))


def filter_data(data):
    return data[(data >= -5) & (data <= 5)]

uniform_data = filter_data(torch.FloatTensor(num_samples).uniform_(-5, 5))
gaussian_data = filter_data(torch.randn(num_samples) * 0.5)
t_distribution_data1 = filter_data(torch.from_numpy(t.rvs(0.1, size=num_samples).astype(np.float32)))
t_distribution_data2 = filter_data(torch.from_numpy(t.rvs(1, size=num_samples).astype(np.float32)))
t_distribution_data3 = filter_data(torch.from_numpy(t.rvs(10, size=num_samples).astype(np.float32)))
laplace_data = filter_data(torch.from_numpy(laplace.rvs(loc=0, size=num_samples).astype(np.float32)))

# 初始化量化器
def initialize_quantizer():
    return FPQuantizer(
        n_bits=4,  # 总位数设置为4
        mantissa_bits=1,  # 尾数位设置为1
        learn_maxval=True,
        maxval=5.0, # 初始最大值
        set_maxval=True
    )

# 定义计算量化损失的函数（使用均方误差）
def quantization_loss_mse(original, quantized):
    return torch.mean((original - quantized) ** 2)

# 优化并找到最佳 maxval
def optimize_maxval(data, epochs=1000, lr=0.1):
    quantizer = initialize_quantizer()
    # optimizer = optim.Adam([quantizer.maxval], lr=lr)
    optimizer = RangerAdaBelief([quantizer.maxval], lr=lr)
    progress_bar = tqdm(range(epochs), desc="Optimizing Maxval")

    for epoch in progress_bar:
        optimizer.zero_grad()
        quantized_data = quantizer(data)
        loss = quantization_loss_mse(data, quantized_data)
        if torch.isnan(loss):
            print(data)
            print(quantized_data)
            print(f"NaN loss encountered at epoch {epoch}")
            break
        loss.backward()
        optimizer.step()
        progress_bar.set_postfix({"loss": loss.item(), "maxval": quantizer.maxval.item()})


    return quantizer.maxval.item()


def generate_fp_lines(num_total_bits, num_exponent_bits, bias):
    return generate_all_values_fp1(num_total_bits, num_exponent_bits, bias)
    return generate_all_values_fp(num_total_bits, num_exponent_bits, bias)


# 绘制和保存优化后的量化图
def plot_optimized_quantization(data, title, save_path, maxval):
    quantizer = FPQuantizer(n_bits=4, mantissa_bits=1, maxval=maxval)
    quantized_data = quantizer(data)
    loss = quantization_loss_mse(data, quantized_data).item()
    plt.figure(figsize=(10, 4))
    count, bins, ignored = plt.hist(data.numpy(), bins=30, alpha=0.6, color='b', density=True)
    
    # 计算所有FP位置并添加红线
    # fp_values = generate_fp_lines(4, 1, 1)  # 调整这些参数以匹配你的FP配置
    # print(fp_values)
    # for val in fp_values:
    #     plt.axvline(x=val, color='red', linestyle='--', linewidth=0.5)
    
    plt.title(f'{title}\nOptimized maxval: {maxval:.2f}, Loss: {loss:.3f}')
    plt.xlabel('Value')
    plt.ylabel('Density')
    plt.grid(True)
    plt.savefig(save_path)
    plt.close()


folder_path = 'explore_images1/'  # 确保此文件夹存在
datasets = [('Uniform', uniform_data), 
            ('Gaussian', gaussian_data), 
            ('T Distribution-100.0', t_distribution_data1),
             ('T Distribution-1.0', t_distribution_data2),
              ('T Distribution-10.0', t_distribution_data3), 
              ('Laplace', laplace_data)
              ]

for name, data in datasets:
    best_maxval = optimize_maxval(data)
    plot_path = f'{folder_path}{name.lower().replace(" ", "_")}_distribution.png'
    plot_optimized_quantization(data, f'Quantize the W with {name} Distribution', plot_path, best_maxval)
