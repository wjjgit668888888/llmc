#!/usr/bin/env python
import torch
import torch.nn as nn
from quantization.quantizers.base_quantizers import QuantizerBase
import numpy as np
from itertools import product
from torch.autograd import Function
from quantization.quantizers.rounding_utils import round_ste_func

def decode_binary_str(F_str):
    F = sum([2 ** -(i + 1) * int(a) for i, a in enumerate(F_str)]) * 2 ** len(F_str)
    return F

def quantize_to_fp4_ste_MM(
    x_float: torch.Tensor,
    n_bits: int,
    maxval: torch.Tensor,
    num_mantissa_bits: torch.Tensor,
    sign_bits: int,
) -> torch.Tensor:
    M = torch.clamp(round_ste_func(num_mantissa_bits), 1, n_bits - sign_bits)
    E = n_bits - sign_bits - M

    if maxval.shape[0] != 1 and len(maxval.shape) != len(x_float.shape):
        maxval = maxval.view([-1] + [1] * (len(x_float.shape) - 1))
    bias = 2**E - torch.log2(maxval) + torch.log2(2 - 2 ** (-M)) - 1

    minval = -maxval if sign_bits == 1 else torch.zeros_like(maxval)
    xc = torch.min(torch.max(x_float, minval), maxval)

    log_scales = torch.clamp((torch.floor(torch.log2(torch.abs(xc)) + bias)).detach(), 1.0)
    scales = 2.0 ** (log_scales - M - bias)

    result = round_ste_func(xc / scales) * scales
    return result

class FP4Quantizer(QuantizerBase):
    """
    4-bit Floating Point Quantizer
    """

    def __init__(
        self,
        *args,
        scale_domain=None,
        mantissa_bits=1,  # Default mantissa bits changed to 1
        maxval=3,
        set_maxval=False,
        learn_maxval=False,
        learn_mantissa_bits=False,
        mse_include_mantissa_bits=True,
        allow_unsigned=False,
        **kwargs,
    ):
        self.n_bits = 4  # Total bits changed to 4 for FP4
        super().__init__(*args, **kwargs)
        print(self.n_bits)
        print("skdfjkdjfkdkfj")
        self.mantissa_bits = mantissa_bits
        self.ebits = self.n_bits - self.mantissa_bits - 1
        self.default_bias = 2 ** (self.ebits - 1)
        default_maxval = (2 - 2 ** (-self.mantissa_bits)) * 2 ** (2**self.ebits - 1 - self.default_bias)
        self.maxval = maxval if maxval is not None else default_maxval
        self.maxval = torch.Tensor([self.maxval])
        self.mantissa_bits = torch.Tensor([float(self.mantissa_bits)])
        self.set_maxval = set_maxval
        self.learning_maxval = learn_maxval
        self.learning_mantissa_bits = learn_mantissa_bits
        self.mse_include_mantissa_bits = mse_include_mantissa_bits
        self.allow_unsigned = allow_unsigned
        self.sign_bits = 1

    def forward(self, x_float):
        if self.maxval.device != x_float.device:
            self.maxval = self.maxval.to(x_float.device)
        if self.mantissa_bits.device != x_float.device:
            self.mantissa_bits = self.mantissa_bits.to(x_float.device)
        
        return quantize_to_fp4_ste_MM(
            x_float, self.n_bits, self.maxval, self.mantissa_bits, self.sign_bits
        )

    def is_initialized(self):
        return True

    def symmetric(self):
        return False

    def effective_bit_width(self):
        return None

    def extra_repr(self):
        return f"Exponent: {self.ebits} bits; Mantissa: {self.mantissa_bits.item()} bits; Bias: {self.default_bias}"
