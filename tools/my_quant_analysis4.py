from transformers import AutoModelForCausalLM, AutoConfig
from loguru import logger
from tqdm import tqdm

import gc
import os
import torch
import functools
import argparse
import sys
import numpy as np
from scipy.stats import t as t_distribution, norm
import random
import json


sys.path.append("..")
import matplotlib.pyplot as plt
import torch.nn as nn
from mpl_toolkits.mplot3d import Axes3D

from llmc.data import BaseTokenizer, BaseDataset
from llmc.utils.registry_factory import MODEL_REGISTRY
from llmc.models import *
from llmc.utils import seed_all, check_config, mkdirs
from llmc.compression.quantization import Quantizer
from llmc.compression.quantization import FakeQuantLinear
from matrics_util import matmul_hadU, matmul_random_orthogonal, random_orthogonal_matrix

from llmc.eval import PerplexityEval

from quantization.quantizers.fp8_quantizer import FPQuantizer
from quantization.quantizers.fp4_quantizer import FP4Quantizer

fp_quantizer = FPQuantizer(
        n_bits=4,  # 总位数设置为4
        mantissa_bits=1,  # 尾数位设置为1
        # maxval=3,
        # set_maxval=False,
    )

def apply_fp_quantizer(model):
    # fp_quantizer = FP4Quantizer(
    #     n_bits=4,  # 总位数设置为4
    #     mantissa_bits=2,  # 尾数位设置为1
    #     # maxval=3,
    #     # set_maxval=False,
    # )

    with torch.no_grad():
         for i in tqdm(range(len(model.blocks))):
            block = model.blocks[i]
            block.cuda()

            if hasattr(block, 'self_attn'):
                q_proj_weight = block.self_attn.q_proj.weight
                k_proj_weight = block.self_attn.k_proj.weight
                v_proj_weight = block.self_attn.v_proj.weight

                block.self_attn.q_proj.weight.copy_(fp_quantizer.forward(q_proj_weight))
                block.self_attn.k_proj.weight.copy_(fp_quantizer.forward(k_proj_weight))
                block.self_attn.v_proj.weight.copy_(fp_quantizer.forward(v_proj_weight))

            block.cpu()
            torch.cuda.empty_cache()

def quantize(data):
    quantized_data = fp_quantizer.forward(data)
    return quantized_data

# 定义计算量化损失的函数（使用均方误差）
def quantization_loss_mse(original, quantized):
    return torch.mean((original - quantized) ** 2).item()

def evaluate_ppl(model, tokenizer, dataset_config):
    eval_config = {
        "name": dataset_config['name'],  # 确保这里有具体的数据集名称，例如 'wikitext2'
        "path": dataset_config['path'], 
        "bs": dataset_config['bs'], 
        "seq_len": dataset_config['seq_len'],
        "download": dataset_config.get('download', False),  # 你可以设置默认值
        "inference_per_block": dataset_config.get('inference_per_block', False)  # 可选配置项的默认值
    }
    ppl_evaluator = PerplexityEval(tokenizer.get_tokenizer(), eval_config)
    ppl = ppl_evaluator.eval(model)
    return ppl


def apply_hadamard_transform(tensor):
    # 假设tensor的形状是正确的，并且我们正在应用Hadamard变换到最后一个维度
    # tensor = tensor.cpu().numpy()  # Convert to numpy if not already
    # transformed_tensor = matmul_hadU(torch.from_numpy(tensor)).numpy()  # Apply transformation and convert back to numpy
    return matmul_hadU(tensor)


def draw(save_path, save_name, X, Y1, Y2):
    print(f"Drawing and saving plot to {save_path}/{save_name}.jpg")
    fig, ax = plt.subplots()
    if len(X) > 1:
        ax.plot(X, Y1, label='Min Value')
        ax.plot(X, Y2, label='Max Value')
        plt.xlabel("Channel")
    else:
        ax.scatter(X, Y1, color='blue', label='Min Value')  # 散点图用于单个数据点
        ax.scatter(X, Y2, color='red', label='Max Value')
        plt.xlabel("Tensor")
    plt.ylabel("Value")
    plt.title(save_name)
    plt.legend()
    fig.savefig(f"{save_path}/{save_name}.jpg")
    plt.close(fig)

def draw_3d_bar_chart(save_path, save_name, data, type,):
    print(f"Drawing and saving 3D surface to {save_path}/{save_name}.png")
    print(f"Data shape: {data.shape}, Data stats: Min={np.min(data)}, Max={np.max(data)}")


    if data.ndim == 3:
        if data.shape[0] == 1:
            data = data.squeeze(0)
        else:
            # 对第一维求平均值
            print(f"data.shape[0] != 1,is {data.shape[0]}")
            exit()
            data = np.mean(data, axis=0)
    print(f"Data shape: {data.shape}")
    
    # 检查是否仍然不是二维，如果是，直接报错
    if data.ndim != 2:
        raise ValueError(f"Data for 3D surface plot must be 2-dimensional, got shape {data.shape}")
    
    if type == "input":
        print("ababa")
    # 创建坐标网格
    
    X = np.arange(data.shape[1])  # 沿宽度
    Y = np.arange(data.shape[0])  # 沿高度
    X, Y = np.meshgrid(X, Y)
    Z = data 

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(X, Y, Z, cmap='coolwarm',  antialiased=False)  # rstride=8, cstride=8,
    ax.set_zlim(np.min(Z), np.max(Z))  # 设置 Z 轴的高度限制为数据的最小值和最大值
    

    # 设置坐标轴标签
    ax.set_xlabel('Channel')  # 交换标签
    # ax.set_ylabel('In channel')   # 交换标签
    ax.set_zlabel('Value')

    # 设置视角
    ax.view_init(elev=30, azim=-45)   # 调整视角

    # 添加色标
    # fig.colorbar(surf)

    plt.title(save_name)
    fig.savefig(f"{save_path}/{save_name}.png", dpi=600)
    plt.close(fig)

    print(f"Finished drawing {save_name}")

# 初始化一个全局字典来存储所有层的参数
all_t_params = {}
def draw_weight_distribution(save_path, save_name, data, plot_type, sample_size=800000, bins=500, alpha=0.6):
    print(f"Drawing and saving distribution to {save_path}/{save_name}.png")
    print(f"Data shape: {data.shape}, Data stats: Min={np.min(data)}, Max={np.max(data)}")
    
    # 确保输入数据是 NumPy 数组
    if isinstance(data, torch.Tensor):
        data = data.detach().cpu().numpy()
    
    # 如果数据是三维且第一个维度为1，可以去掉这个批次维度
    if data.ndim == 3:
        if data.shape[0] == 1:
            data = data.squeeze(0)
        else:
            # 对第一维求平均值
            data = np.mean(data, axis=0)
    print(f"Data shape after possible squeeze/mean: {data.shape}")

    # 检查是否仍然不是二维，如果是，直接报错
    if data.ndim != 2:
        raise ValueError(f"Data for distribution plot must be 2-dimensional, got shape {data.shape}")

    # 展平数据以绘制直方图
    flattened_data = data.flatten()

    # 抽样减少数据量
    if len(flattened_data) > sample_size:
        flattened_data = random.sample(list(flattened_data), sample_size)
    
    # 数据标准化处理
    flattened_data = (flattened_data - np.mean(flattened_data)) / np.std(flattened_data)

    # 绘制直方图并计算密度
    fig, ax = plt.subplots(figsize=(10, 4))  # 调整图形尺寸
    n, bins, patches = ax.hist(flattened_data, bins=bins, color='blue', alpha=alpha, label='Data', density=True)

    # 拟合 t 分布
    t_params = t_distribution.fit(flattened_data)
    print(f"Fitted t-distribution parameters: {t_params}")
    t_df, t_loc, t_scale = t_params
    print(f"t-distribution center: {t_loc}")

    # 拟合高斯分布
    norm_params = norm.fit(flattened_data)
    print(f"Fitted Gaussian distribution parameters: {norm_params}")
    # norm_loc, norm_scale = norm_params
    norm_loc, norm_scale = map(float, norm_params)  # 转换为 Python 原生 float 类型

    # 绘制拟合曲线
    x = np.linspace(bins[0], bins[-1], 1000)
    
    t_pdf_fitted = t_distribution.pdf(x, t_df, t_loc, t_scale)
    norm_pdf_fitted = norm.pdf(x, norm_loc, norm_scale)
    
    ax.plot(x, t_pdf_fitted, 'r-', label='Fitted t-distribution')
    ax.plot(x, norm_pdf_fitted, 'g--', label='Fitted Gaussian distribution')

    if plot_type == "weight":
        ax.set_xlabel('Value of weight')
        ax.set_ylabel('Density')
        ax.set_title('Weight distribution with fitted curves')
    else:
        ax.set_xlabel('Value of input')
        ax.set_ylabel('Density')
        ax.set_title('input distribution with fitted curves')

    ax.legend()

    plt.savefig(f"{save_path}/{save_name}.png")
    plt.close(fig)

    print(f"Finished drawing {save_name}")

     # 保存每一层的 t 分布参数到全局字典中
    all_t_params[save_name] = {
        "t_df": t_df,
        "t_loc": t_loc,
        "t_scale": t_scale,
        "norm_loc": norm_loc,
        "norm_scale": norm_scale
    }

    print(f"Saved t-distribution parameters for {save_name}")



def analysis_block_outlier(res, t_res, args):
    for name in res:
        if args.quant_mode == "per_tensor":
            # Directly use the entire tensor
            tensor = res[name].detach().cpu().numpy()  # Assuming the data is 2D weight matrix (out_features x in_features)
            print(f"The shape of the tensor is: {tensor.shape}")
            abs_tensor = np.abs(tensor)
            max_val = np.max(abs_tensor)
            min_val = np.min(abs_tensor)
        else:  # per_channel
            tensor = res[name].detach().cpu().numpy() 
            # tensor = res[name].mean(dim=0)
            # print(tensor)
            max_val = np.max(tensor)
            min_val = np.min(tensor)
            # print(max_val)

        logger.info(f"The original model {name}'s max value: {max_val}")
        logger.info(f"The original model {name}'s min value: {min_val}")

        # Handle transformed model similarly
        if args.quant_mode == "per_tensor":
            # Directly use the entire tensor
            t_tensor = t_res[name].detach().cpu().numpy()  # Assuming the data is 2D weight matrix (out_features x in_features)
            print(f"The shape of the t_tensor is: {t_tensor.shape}")
            t_abs_tensor = np.abs(t_tensor)
            t_max_val = np.max(t_abs_tensor)
            t_min_val = np.min(t_abs_tensor)
        else:  # per_channel
            t_tensor = t_res[name].detach().cpu().numpy() 
            # tensor = res[name].mean(dim=0)
            # print(tensor)
            t_max_val = np.max(t_tensor)
            t_min_val = np.min(t_tensor)
            # print(max_val)

        logger.info(f"The transformed model {name}'s t_max value: {t_max_val}")
        logger.info(f"The transformed model {name}'s t_min value: {t_min_val}")
        
        if args.draw:
            save_path = os.path.join(args.save_path, "tensor" if args.quant_mode == "per_tensor" else "channel")
            if not os.path.exists(args.save_path):
                mkdirs(save_path)
            if args.quant_mode == "per_tensor":
                draw_3d_bar_chart(save_path, f"{name}_original", abs_tensor, args.type)
                draw_3d_bar_chart(save_path, f"{name}_rotate", t_abs_tensor, args.type)
                # draw_3d_surface(save_path, f"{name}_original", abs_tensor)
                # draw_heatmap(save_path, f"{name}_transformed", t_tensor)
            elif args.quant_mode == "per_channel":
                # X = range(tensor.shape[-1])
                # print("X shape:", len(X))  # 输出 X 的长度
                # print("Y1 shape:", np.shape(min_val))  # 输出 Y1 的形状
                # print("Y2 shape:", np.shape(max_val))  # 输出 Y2 的形状
                draw_weight_distribution(save_path, f"{name}_original", tensor, args.type)
                draw_weight_distribution(save_path, f"{name}_rotate", t_tensor, args.type)


def save_all_t_params(save_path, filename="all_t_params.json"):
    with open(os.path.join(save_path, filename), "w") as json_file:
        json.dump(all_t_params, json_file, indent=4)
    print(f"Saved all t-distribution parameters to {save_path}/{filename}")


def register_hook(block, idx, args):
    hooks = []
    for name, m in block.named_modules():
        if not args.cosine:
            if isinstance(m, torch.nn.Linear):
                if args.type == "input":
                    hooks.append(
                        m.register_forward_hook(
                            functools.partial(
                                stat_input_hook, name=name, idx=idx, args=args
                            )
                        )
                    )
                else:
                    hooks.append(
                        m.register_forward_hook(
                            functools.partial(
                                stat_weight_hook, name=name, idx=idx, args=args
                            )
                        )
                    )
        else:
            pass
    return hooks


def stat_input_hook(m, x, y, name, idx, args):
    if isinstance(x, tuple):
        x = x[0]
    layer_name = f"block_{idx}.{name}"
    # print(f"Data shape: {x.shape}")
    # print(f"Data shape: {x[0].shape}")
    if t:
        t_res[layer_name] = x
    else:
        res[layer_name] = x

def stat_weight_hook(m, x, y, name, idx, args):
    if isinstance(x, tuple):
        x = x[0]
    y = m.weight.cpu()
    layer_name = f"block_{idx}.{name}"
    # print(f"weight shape: {y.shape}")
    if t:
        t_res[layer_name] = y
    else:
        res[layer_name] = y


def stat_output_hook(m, x, y, name, idx, args):
    if isinstance(y, tuple):
        y = y[0]
    layer_name = f"block_{idx}.{name}"
    if t:
        t_res[layer_name] = y
    else:
        res[layer_name] = y


def block_forward(block, input_data, input_kwargs):
    output = []

    for i in range(len(input_data)):
        input_data[i] = input_data[i].to(
            device=next(block.parameters()).device,
            dtype=next(block.parameters()).dtype,
        )
        if "attention_mask" in input_kwargs[i]:
            input_kwargs[i]["attention_mask"] = input_kwargs[i]["attention_mask"].cuda()
        with torch.no_grad():
            out = block(input_data[i], **input_kwargs[i])[0]
            output.append(out)
    return output


class analysis_quanter(Quantizer):
    def __init__(self, bit, symmetric, granularity, **kwargs):
        super().__init__(bit, symmetric, granularity, **kwargs)

    def fake_quant_weight_dynamic(self, module, args={}):
        weight = module.weight
        if "int_indices" in args:
            if self.granularity == "per_group":
                assert len(args["int_indices"]) % self.group_size == 0
            q_weight = weight[:, args["int_indices"]]
            fp_weight = weight[:, args["fp_indices"]]

        elif "dim" in args and "ic" in args["dim"]:
            q_weight = weight.T
        else:
            q_weight = weight

        if "current_bit" in args:
            org_bit = self.bit
            self.bit = args["current_bit"]

        org_w_shape = q_weight.shape
        org_w_dtype = q_weight.dtype
        q_weight, scales, zeros, max_int, min_int = self.get_tensor_qparams(
            q_weight, args
        )

        q_weight = self.quant_dequant(q_weight, scales, zeros, max_int, min_int)
        q_weight = self.restore_tensor(q_weight, org_w_shape).to(org_w_dtype)

        if "current_bit" in args:
            self.bit = org_bit

        if "int_indices" in args:
            mix_weight = torch.zeros_like(weight)
            mix_weight[:, args["int_indices"]] = q_weight
            mix_weight[:, args["fp_indices"]] = fp_weight
            return mix_weight

        elif "dim" in args and "ic" in args["dim"]:
            q_weight = q_weight.T

        return q_weight

import yaml

def load_config(file_path):
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

mse_results = {}  # 存储各层的 MSE 结果
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_name", type=str)
    parser.add_argument("--data_path", type=str)
    parser.add_argument("--n_samples", type=int, default=128)
    parser.add_argument("--bs", type=int, default=-1)
    parser.add_argument("--seq_len", type=int, default=512)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--preproc", type=str, default="general")
    parser.add_argument("--save_path", type=str, default="./save")
    parser.add_argument("--draw", action="store_true")
    parser.add_argument("--cosine", action="store_true")
    parser.add_argument("--model_type", type=str, required=True)
    parser.add_argument("--model_path", type=str, required=True)
    parser.add_argument("--t_model_path", type=str)
    parser.add_argument("--torch_dtype", type=str, default="auto")

    parser.add_argument("--w_only", action="store_true")
    parser.add_argument("--wbit", type=int, default=4)
    parser.add_argument("--wsym", action="store_true")
    parser.add_argument("--wgra", type=str, default="per_group")
    parser.add_argument("--group_size", type=int, default=128)

    parser.add_argument("--abit", type=int, default=4)
    parser.add_argument("--asym", action="store_true")
    parser.add_argument("--agra", type=str, default="per_token")

    parser.add_argument("--quant_mode", type=str, default="per_channel", choices=["per_channel", "per_tensor"])
    parser.add_argument("--type", type=str, default="input", choices=["input", "weight"])

    parser.add_argument("--enable_ppl", action="store_true", help="Enable PPL testing after quantization")
    parser.add_argument("--enable_draw", action="store_true")
    parser.add_argument("--enable_mse", action="store_true")

    args = parser.parse_args()
    
    logger.info(f"args : {args}")

    calib_cfg = {
        "name": args.dataset_name,
        "download": False,
        "path": args.data_path,
        "n_samples": args.n_samples,
        "bs": args.bs,
        "seq_len": args.seq_len,
        "preproc": args.preproc,
        "seed": args.seed,
    }

    model_config = {
        "type": args.model_type,
        "path": args.model_path,
        "torch_dtype": args.torch_dtype,
    }



    model = MODEL_REGISTRY[args.model_type](args.model_path, args.torch_dtype)
    t_model = MODEL_REGISTRY[args.model_type](args.t_model_path, args.torch_dtype)

    logger.info(model)

    tokenizer = BaseTokenizer(args.model_path, tokenizer_mode='gpt')
    dataset = BaseDataset(tokenizer.get_tokenizer(), calib_cfg)

    calib_data = dataset.get_calib_dataset()

    model.collect_first_block_input(calib_data)
    t_model.collect_first_block_input(calib_data)

    fp_inps = model.get_first_block_input()
    t_fp_inps = t_model.get_first_block_input()

    res = {}
    t_res = {}

    if args.cosine:
        wquanter = analysis_quanter(
            bit=args.wbit,
            symmetric=args.wsym,
            granularity=args.wgra,
            group_size=args.group_size,
        )

        if not args.w_only:
            aquanter = Quantizer(
                bit=args.abit, symmetric=args.asym, granularity=args.agra
            )

        params_dict = {}
        params_dict["w_qdq"] = wquanter.fake_quant_weight_dynamic
        params_dict["a_qdq"] = None if args.w_only else aquanter.fake_quant_act_dynamic
        t_model.replace_module_all(FakeQuantLinear, params_dict)

    if args.enable_draw:
        with torch.no_grad():
            for i in tqdm(range(len(model.blocks))):
                block = model.blocks[i]
                # t_block = t_model.blocks[i]
                block.cuda()
                # t_block.cuda()

                hooks = register_hook(block, i, args)
                t = False
                fp_inps["data"] = block_forward(block, fp_inps["data"], fp_inps["kwargs"])

                # t_hooks = register_hook(t_block, i, args)
                # t = True
                # t_fp_inps["data"] = block_forward(
                #     t_block, t_fp_inps["data"], t_fp_inps["kwargs"]
                # )

                block.cpu()

                # t_block.cpu()

                for h in hooks:
                    h.remove()

                # for name, tensor in res.items():
                #     t_res[name] = matmul_hadU(tensor)
        
                for name, tensor in res.items():
                    tensor = tensor.to('cuda')  # 确保每个tensor都在GPU上
                    # 应用随机正交变换
                    transformed_tensor, Q = matmul_random_orthogonal(tensor)
                    t_res[name] = transformed_tensor  # 保存变换后的张量
                # for t_h in t_hooks:
                #     t_h.remove()
                # 量化原始和变换后的张量
                if args.enable_mse:
                    save_path = args.save_path
                    if not os.path.exists(save_path):
                            os.makedirs(save_path)  # 创建目录
                    mse_filename = os.path.join(save_path, "mse_results.json")
                    for name in res:
                        # 处理原始数据的量化和MSE计算
                        quantized_res = quantize(res[name])
                        mse_original = quantization_loss_mse(res[name], quantized_res)

                        # 处理变换后数据的量化和MSE计算
                        quantized_t_res = quantize(t_res[name])
                        mse_transformed = quantization_loss_mse(t_res[name], quantized_t_res)
                        
                        # 同时存储两个MSE结果到mse_results中
                        mse_results[name] = {
                            "original_vs_quantized": mse_original,
                            "transformed_vs_quantized": mse_transformed
                        }

                # 保存结果到文件
                with open(mse_filename, 'w') as f:
                    json.dump(mse_results, f, indent=4)
                print(f"已保存 MSE 结果到 {mse_filename}")

                if args.cosine:
                    print(222222);
                    # analysis_block_cosine(res, t_res, args)
                else:
                    print(333333);
                    analysis_block_outlier(res, t_res, args)
                    # 保存所有层的 t 分布参数
                    if args.quant_mode == "per_channel" and args.draw:
                        save_all_t_params(args.save_path)
                res.clear()
                t_res.clear()

                gc.collect()
                torch.cuda.empty_cache()
                exit()
    

    if args.enable_ppl:
        ppl = evaluate_ppl(model, tokenizer, calib_cfg)
        # 遍历模型中的所有层，并找到自注意力层的QKV矩阵
        apply_fp_quantizer(model)
        ppl2 = evaluate_ppl(model, tokenizer, calib_cfg)
        logger.info(f"Model Perplexity before quantization: {ppl}")
        logger.info(f"Model Perplexity after quantization: {ppl2}")