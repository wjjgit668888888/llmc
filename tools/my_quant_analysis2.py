from transformers import AutoModelForCausalLM, AutoConfig
from loguru import logger
from tqdm import tqdm

import gc
import os
import torch
import functools
import argparse
import sys
import numpy as np
from scipy.stats import t as t_distribution, norm
import random


sys.path.append("..")
import matplotlib.pyplot as plt
import torch.nn as nn
from mpl_toolkits.mplot3d import Axes3D

from llmc.data import BaseTokenizer, BaseDataset
from llmc.utils.registry_factory import MODEL_REGISTRY
from llmc.models import *
from llmc.utils import seed_all, check_config, mkdirs
from llmc.compression.quantization import Quantizer
from llmc.compression.quantization import FakeQuantLinear


def draw(save_path, save_name, X, Y1, Y2):
    print(f"Drawing and saving plot to {save_path}/{save_name}.jpg")
    fig, ax = plt.subplots()
    if len(X) > 1:
        ax.plot(X, Y1, label='Min Value')
        ax.plot(X, Y2, label='Max Value')
        plt.xlabel("Channel")
    else:
        ax.scatter(X, Y1, color='blue', label='Min Value')  # 散点图用于单个数据点
        ax.scatter(X, Y2, color='red', label='Max Value')
        plt.xlabel("Tensor")
    plt.ylabel("Value")
    plt.title(save_name)
    plt.legend()
    fig.savefig(f"{save_path}/{save_name}.jpg")
    plt.close(fig)

def draw_3d_bar_chart(save_path, save_name, data, type,):
    print(f"Drawing and saving 3D surface to {save_path}/{save_name}.png")
    print(f"Data shape: {data.shape}, Data stats: Min={np.min(data)}, Max={np.max(data)}")


    if data.ndim == 3:
        if data.shape[0] == 1:
            data = data.squeeze(0)
        else:
            # 对第一维求平均值
            data = np.mean(data, axis=0)
    print(f"Data shape: {data.shape}")
    
    # 检查是否仍然不是二维，如果是，直接报错
    if data.ndim != 2:
        raise ValueError(f"Data for 3D surface plot must be 2-dimensional, got shape {data.shape}")
    
    if type == "input":
        print("ababa")
    # 创建坐标网格
    X = np.arange(data.shape[1])  # 沿宽度
    Y = np.arange(data.shape[0])  # 沿高度
    X, Y = np.meshgrid(X, Y)
    Z = data 

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(X, Y, Z, cmap='coolwarm', edgecolor='none')  
    ax.set_zlim(np.min(Z), np.max(Z))  # 设置 Z 轴的高度限制为数据的最小值和最大值

    # 设置坐标轴标签
    ax.set_xlabel('Out channel')  # 交换标签
    ax.set_ylabel('In channel')   # 交换标签
    ax.set_zlabel('Value')

    # 设置视角
    ax.view_init(elev=30, azim=-45)   # 调整视角

    # 添加色标
    fig.colorbar(surf)

    plt.title(save_name)
    fig.savefig(f"{save_path}/{save_name}.png")
    plt.close(fig)

    print(f"Finished drawing {save_name}")

def draw_weight_distribution(save_path, save_name, data, plot_type, sample_size=800000, bins=500, alpha=0.6):
    print(f"Drawing and saving weight distribution to {save_path}/{save_name}.png")
    print(f"Data shape: {data.shape}, Data stats: Min={np.min(data)}, Max={np.max(data)}")
    
    # 确保输入数据是 NumPy 数组
    if isinstance(data, torch.Tensor):
        data = data.detach().cpu().numpy()
    
    # 如果数据是三维且第一个维度为1，可以去掉这个批次维度
    if data.ndim == 3:
        if data.shape[0] == 1:
            data = data.squeeze(0)
        else:
            # 对第一维求平均值
            data = np.mean(data, axis=0)
    print(f"Data shape after possible squeeze/mean: {data.shape}")

    # 检查是否仍然不是二维，如果是，直接报错
    if data.ndim != 2:
        raise ValueError(f"Data for weight distribution plot must be 2-dimensional, got shape {data.shape}")

    # 展平数据以绘制直方图
    flattened_data = data.flatten()

    # 抽样减少数据量
    if len(flattened_data) > sample_size:
        flattened_data = random.sample(list(flattened_data), sample_size)

    # 标准化数据
    flattened_data = (flattened_data - np.mean(flattened_data)) / np.std(flattened_data)

    # 绘制直方图并计算 counts
    fig, ax = plt.subplots(figsize=(10, 4))  # 调整图形尺寸
    n, bins, patches = ax.hist(flattened_data, bins=bins, color='blue', alpha=alpha, label='Data')
    
    # 对 counts 取 log
    n_log = np.log(n + 1)  # 添加 1 以避免 log(0)

    # 绘制直方图的 log 变换
    ax.clear()  # 清除当前的直方图
    ax.bar(bins[:-1], n_log, width=(bins[1] - bins[0]), color='blue', alpha=alpha, label='Data (log(count))')

    # 拟合 t 分布
    t_params = t_distribution.fit(flattened_data)
    print(f"Fitted t-distribution parameters: {t_params}")
    t_df, t_loc, t_scale = t_params
    print(f"t-distribution center: {t_loc}")

    # 拟合高斯分布
    norm_params = norm.fit(flattened_data)
    print(f"Fitted Gaussian distribution parameters: {norm_params}")
    norm_loc, norm_scale = norm_params

    # 绘制拟合曲线
    x = np.linspace(bins[0], bins[-1], 1000)
    
    t_pdf_fitted = t_distribution.pdf(x, t_df, t_loc, t_scale)
    norm_pdf_fitted = norm.pdf(x, norm_loc, norm_scale)
    
    # 由于直方图取了 log，曲线也需要做相应调整
    ax.plot(x, np.log(t_pdf_fitted * max(n) * (bins[1] - bins[0]) + 1), 'r-', label='Fitted t-distribution')
    ax.plot(x, np.log(norm_pdf_fitted * max(n) * (bins[1] - bins[0]) + 1), 'g--', label='Fitted Gaussian distribution')

    ax.set_xlabel('Value of weight')
    ax.set_ylabel('Log(Count)')
    ax.set_title('Weight distribution with fitted curves (log-transformed counts)')

    ax.legend()

    plt.savefig(f"{save_path}/{save_name}.png")
    plt.close(fig)

    print(f"Finished drawing {save_name}")




def analysis_block_outlier(res, t_res, args):
    for name in res:
        
        if args.quant_mode == "per_tensor":
            # Directly use the entire tensor
            tensor = res[name].detach().cpu().numpy()  # Assuming the data is 2D weight matrix (out_features x in_features)
            print(f"The shape of the tensor is: {tensor.shape}")
            abs_tensor = np.abs(tensor)
            max_val = np.max(abs_tensor)
            min_val = np.min(abs_tensor)
        else:  # per_channel
            tensor = res[name].detach().cpu().numpy() 
            # tensor = res[name].mean(dim=0)
            # print(tensor)
            max_val = np.max(tensor)
            min_val = np.min(tensor)
            # print(max_val)

        logger.info(f"The original model {name}'s max value: {max_val}")
        logger.info(f"The original model {name}'s min value: {min_val}")

        # Handle transformed model similarly
        '''
        t_tensor = t_res[name].detach().cpu().numpy()
        if args.quant_mode == "per_tensor":
            abs_t_tensor = np.abs(t_tensor)
            t_max_val = np.max(abs_t_tensor)
            t_min_val = np.min(abs_t_tensor)
        else:
            abs_t_tensor = np.abs(t_tensor.mean(axis=1))
            t_max_val = np.max(abs_t_tensor)
            t_min_val = np.min(abs_t_tensor)

        logger.info(f"The transformed model {name}'s max value: {t_max_val}")
        logger.info(f"The transformed model {name}'s min value: {t_min_val}")
        '''
        if args.draw:
            save_path = os.path.join(args.save_path, "tensor_outlier" if args.quant_mode == "per_tensor" else "outlier")
            if not os.path.exists(args.save_path):
                mkdirs(save_path)
            if args.quant_mode == "per_tensor":
                draw_3d_bar_chart(save_path, f"{name}_original", abs_tensor, args.type)
                # draw_3d_surface(save_path, f"{name}_original", abs_tensor)
                # draw_heatmap(save_path, f"{name}_transformed", t_tensor)
            elif args.quant_mode == "per_channel":
                # X = range(tensor.shape[-1])
                # print("X shape:", len(X))  # 输出 X 的长度
                # print("Y1 shape:", np.shape(min_val))  # 输出 Y1 的形状
                # print("Y2 shape:", np.shape(max_val))  # 输出 Y2 的形状
                draw_weight_distribution(save_path, f"{name}_original", tensor, args.type)

def register_hook(block, idx, args):
    hooks = []
    for name, m in block.named_modules():
        if not args.cosine:
            if isinstance(m, torch.nn.Linear):
                if args.type == "input":
                    hooks.append(
                        m.register_forward_hook(
                            functools.partial(
                                stat_input_hook, name=name, idx=idx, args=args
                            )
                        )
                    )
                else:
                    hooks.append(
                        m.register_forward_hook(
                            functools.partial(
                                stat_weight_hook, name=name, idx=idx, args=args
                            )
                        )
                    )
        else:
            pass
    return hooks


def stat_input_hook(m, x, y, name, idx, args):
    if isinstance(x, tuple):
        x = x[0]
    layer_name = f"block_{idx}.{name}"
    # print(f"Data shape: {x.shape}")
    # print(f"Data shape: {x[0].shape}")
    if t:
        t_res[layer_name] = x
    else:
        res[layer_name] = x

def stat_weight_hook(m, x, y, name, idx, args):
    if isinstance(x, tuple):
        x = x[0]
    y = m.weight.cpu()
    layer_name = f"block_{idx}.{name}"
    # print(f"weight shape: {y.shape}")
    if t:
        t_res[layer_name] = y
    else:
        res[layer_name] = y


def stat_output_hook(m, x, y, name, idx, args):
    if isinstance(y, tuple):
        y = y[0]
    layer_name = f"block_{idx}.{name}"
    if t:
        t_res[layer_name] = y
    else:
        res[layer_name] = y


def block_forward(block, input_data, input_kwargs):
    output = []

    for i in range(len(input_data)):
        input_data[i] = input_data[i].to(
            device=next(block.parameters()).device,
            dtype=next(block.parameters()).dtype,
        )
        if "attention_mask" in input_kwargs[i]:
            input_kwargs[i]["attention_mask"] = input_kwargs[i]["attention_mask"].cuda()
        with torch.no_grad():
            out = block(input_data[i], **input_kwargs[i])[0]
            output.append(out)
    return output


class analysis_quanter(Quantizer):
    def __init__(self, bit, symmetric, granularity, **kwargs):
        super().__init__(bit, symmetric, granularity, **kwargs)

    def fake_quant_weight_dynamic(self, module, args={}):
        weight = module.weight
        if "int_indices" in args:
            if self.granularity == "per_group":
                assert len(args["int_indices"]) % self.group_size == 0
            q_weight = weight[:, args["int_indices"]]
            fp_weight = weight[:, args["fp_indices"]]

        elif "dim" in args and "ic" in args["dim"]:
            q_weight = weight.T
        else:
            q_weight = weight

        if "current_bit" in args:
            org_bit = self.bit
            self.bit = args["current_bit"]

        org_w_shape = q_weight.shape
        org_w_dtype = q_weight.dtype
        q_weight, scales, zeros, max_int, min_int = self.get_tensor_qparams(
            q_weight, args
        )

        q_weight = self.quant_dequant(q_weight, scales, zeros, max_int, min_int)
        q_weight = self.restore_tensor(q_weight, org_w_shape).to(org_w_dtype)

        if "current_bit" in args:
            self.bit = org_bit

        if "int_indices" in args:
            mix_weight = torch.zeros_like(weight)
            mix_weight[:, args["int_indices"]] = q_weight
            mix_weight[:, args["fp_indices"]] = fp_weight
            return mix_weight

        elif "dim" in args and "ic" in args["dim"]:
            q_weight = q_weight.T

        return q_weight

import yaml

def load_config(file_path):
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset_name", type=str)
    parser.add_argument("--data_path", type=str)
    parser.add_argument("--n_samples", type=int, default=128)
    parser.add_argument("--bs", type=int, default=-1)
    parser.add_argument("--seq_len", type=int, default=512)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--preproc", type=str, default="general")
    parser.add_argument("--save_path", type=str, default="./save")
    parser.add_argument("--draw", action="store_true")
    parser.add_argument("--cosine", action="store_true")
    parser.add_argument("--model_type", type=str, required=True)
    parser.add_argument("--model_path", type=str, required=True)
    parser.add_argument("--t_model_path", type=str)
    parser.add_argument("--torch_dtype", type=str, default="auto")

    parser.add_argument("--w_only", action="store_true")
    parser.add_argument("--wbit", type=int, default=4)
    parser.add_argument("--wsym", action="store_true")
    parser.add_argument("--wgra", type=str, default="per_group")
    parser.add_argument("--group_size", type=int, default=128)

    parser.add_argument("--abit", type=int, default=4)
    parser.add_argument("--asym", action="store_true")
    parser.add_argument("--agra", type=str, default="per_token")

    parser.add_argument("--quant_mode", type=str, default="per_channel", choices=["per_channel", "per_tensor"])
    parser.add_argument("--type", type=str, default="input", choices=["input", "weight"])

    args = parser.parse_args()
    
    logger.info(f"args : {args}")

    calib_cfg = {
        "name": args.dataset_name,
        "download": False,
        "path": args.data_path,
        "n_samples": args.n_samples,
        "bs": args.bs,
        "seq_len": args.seq_len,
        "preproc": args.preproc,
        "seed": args.seed,
    }

    model_config = {
        "type": args.model_type,
        "path": args.model_path,
        "torch_dtype": args.torch_dtype,
    }

    model = MODEL_REGISTRY[args.model_type](args.model_path, args.torch_dtype)
    t_model = MODEL_REGISTRY[args.model_type](args.t_model_path, args.torch_dtype)

    logger.info(model)

    tokenizer = BaseTokenizer(args.model_path, tokenizer_mode='gpt')
    dataset = BaseDataset(tokenizer.get_tokenizer(), calib_cfg)

    calib_data = dataset.get_calib_dataset()

    model.collect_first_block_input(calib_data)
    t_model.collect_first_block_input(calib_data)

    fp_inps = model.get_first_block_input()
    t_fp_inps = t_model.get_first_block_input()

    res = {}
    t_res = {}

    if args.cosine:
        wquanter = analysis_quanter(
            bit=args.wbit,
            symmetric=args.wsym,
            granularity=args.wgra,
            group_size=args.group_size,
        )

        if not args.w_only:
            aquanter = Quantizer(
                bit=args.abit, symmetric=args.asym, granularity=args.agra
            )

        params_dict = {}
        params_dict["w_qdq"] = wquanter.fake_quant_weight_dynamic
        params_dict["a_qdq"] = None if args.w_only else aquanter.fake_quant_act_dynamic
        t_model.replace_module_all(FakeQuantLinear, params_dict)

    with torch.no_grad():
        for i in tqdm(range(len(model.blocks))):
            block = model.blocks[i]
            t_block = t_model.blocks[i]
            block.cuda()
            t_block.cuda()

            hooks = register_hook(block, i, args)
            t = False
            fp_inps["data"] = block_forward(block, fp_inps["data"], fp_inps["kwargs"])

            t_hooks = register_hook(t_block, i, args)
            t = True
            t_fp_inps["data"] = block_forward(
                t_block, t_fp_inps["data"], t_fp_inps["kwargs"]
            )

            block.cpu()

            t_block.cpu()

            for h in hooks:
                h.remove()

            for t_h in t_hooks:
                t_h.remove()
            print(1111111);
            if args.cosine:
                print(222222);
                # analysis_block_cosine(res, t_res, args)
            else:
                print(333333);
                analysis_block_outlier(res, t_res, args)
            res.clear()
            t_res.clear()

            gc.collect()
            torch.cuda.empty_cache()